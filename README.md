AdmissionsCounselor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.6.

### The code in this repo can be seen here: 
https://thebradness.com/angulearn-in-class/dist/AdmissionsCounselor/

### If you pull this down fresh, don't forget to run "npm install" (in the terminal) to fill your node_modules folder with all the things in package.json.
