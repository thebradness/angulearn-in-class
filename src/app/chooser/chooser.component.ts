import {Component, ViewChild} from '@angular/core';
import {StudentChooserComponent} from "../student-chooser/student-chooser.component";
import {SchoolChooserComponent} from "../school-chooser/school-chooser.component";

@Component({
  selector: 'chooser',
  templateUrl: './chooser.component.html',
  styleUrls: ['./chooser.component.less']
})
export class ChooserComponent {

  @ViewChild(StudentChooserComponent, {static: true}) studentChooser: StudentChooserComponent;
  @ViewChild(SchoolChooserComponent, {static: true}) schoolChooser: SchoolChooserComponent;

  constructor() {
  }

  hasText(text: string): boolean {
    return text && text.trim().length > 0; //have to use " > 0" if you specify a boolean return type
  }

  enroll(): void {
    const selectedStudents = this.studentChooser.getSelected();
    this.schoolChooser.enroll(selectedStudents);
    this.studentChooser.enroll();
  }

  hasValidSelection(): boolean {
    const hasStudents = this.studentChooser.getSelectedCount();
    const hasSchool = this.schoolChooser.getSelectedSchool();
    const openEnrollmentSlots = this.schoolChooser.getOpenEnrollmentSlotCount(this.schoolChooser.getSelectedSchool());
    const hasSlots = this.studentChooser.getSelectedCount() <= openEnrollmentSlots;

    return hasStudents && hasSchool && hasSlots;
  }

  buildSelectedStudentsDisplay() {
    const count = this.studentChooser.getSelectedCount();
    const plural = this.plural('Student', count);

    return `${count} ${plural} Selected`;
  }

  plural(word: string, count: number): string {
    if (count == 1) {
      return word;
    }

    return word + 's';
  }
}
