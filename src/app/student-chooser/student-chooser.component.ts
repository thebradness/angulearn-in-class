import { Component, OnInit } from '@angular/core';
import {StudentService} from "../services/student.service";

@Component({
  selector: 'student-chooser',
  templateUrl: './student-chooser.component.html',
  styleUrls: ['./student-chooser.component.less']
})
export class StudentChooserComponent {

  students;
  searchInput;

  constructor(
    private studentService: StudentService
  ) {
    this.subscribeToStudents();
  }

  getStudentsWithSearchInput(): any[] {
    return this.students.filter(s => {
      const hasSearchInput = this.searchInput && this.searchInput.trim().length > 1;
      if (!hasSearchInput) {
        return true; //show everything if no search input
      }

      const firstNameMatch = s.name.first.toLowerCase().includes(this.searchInput.trim().toLowerCase());
      const lastNameMatch = s.name.last.toLowerCase().includes(this.searchInput.trim().toLowerCase());

      return firstNameMatch || lastNameMatch;
    });
  }

  enroll(): void {
    this.getSelected().forEach(s => {
      s.isEnrolled = true;
      s.isSelected = false;
      setTimeout(() => s.isHidden = true, 325);
    });
  }

  deselect(): void {
    this.getSelected().forEach(s => s.isSelected = false);
  }

  getSelected(): any[] {
    return this.students.filter(s => s.isSelected);
  }

  getSelectedCount(): number {
    return this.students && this.students.filter(s => s.isSelected).length;
  }

  buildNameLine(student): string {
    const first = student.name.first;
    const last = student.name.last;

    return `${first} ${last}`;
  }

  buildAddressLine(student): string {
    const loc = student.location;
    return `${loc.city}, ${loc.state} ${loc.country}`;
  }

  private subscribeToStudents(): void {
    this.studentService.data
      .subscribe(students => {
        this.students = students;
        this.students.forEach(s => s.sortName = s.name.last);
      });
  }
}
