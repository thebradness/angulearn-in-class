import {Component, OnInit} from '@angular/core';
import {UniversityService} from "../services/university.service";
import {AppComponent} from "../app.component";

@Component({
  selector: 'school-chooser',
  templateUrl: './school-chooser.component.html',
  styleUrls: ['./school-chooser.component.less']
})
export class SchoolChooserComponent {

  private MAX_ENROLLMENT = 2;

  schools;
  searchInput;
  schoolStudentMap;

  constructor(
    private appComponent: AppComponent,
    private schoolService: UniversityService
  ) {
    this.subscribeToSchools();
    this.schoolStudentMap = this.appComponent.appState;
  }

  buildEnrollmentCounter(school): string {
    return this.getStudentCount(school) + '/' + this.MAX_ENROLLMENT;
  }

  getSchoolsWithSearchInput(): any[] {
    return this.schools.filter(s => {
      const hasSearchInput = this.searchInput && this.searchInput.trim().length > 1;
      if (!hasSearchInput) {
        return true; //show everything if no search input
      }

      return s.name.toLowerCase().includes(this.searchInput.trim().toLowerCase());
    });
  }

  enroll(students: any[]): void {
    this.addEnrolledStudents(students);
    this.deselect();
  }

  deselect(): void {
    const selectedSchool = this.getSelectedSchool();
    selectedSchool && (selectedSchool.isSelected = false);
  }

  schoolIsFull(school: any): boolean {
    return this.getOpenEnrollmentSlotCount(school) == 0;
  }

  getOpenEnrollmentSlotCount(school: any): number {
    return this.MAX_ENROLLMENT - this.getStudentCount(school);
  }

  removeEnrollment(school: any, student: any): void {
    student.isHidden = false;
    setTimeout(() => student.isEnrolled = false, 300);

    const students = this.getEnrolledStudents(school);
    const studentIdx = students.findIndex(s => s.login.uuid == student.login.uuid);
    students.splice(studentIdx, 1);
  }

  getStudentCount(school: any): number {
    const students = this.getEnrolledStudents(school);
    return students.length;
  }

  getEnrolledStudents(school: any): any[] {
    return (school && this.schoolStudentMap[school.name]) || [];
  }

  getSelectedSchool(): any {
    return this.schools.find(s => s.isSelected);
  }

  selectSchool(school): void {
    if (school.isSelected || this.schoolIsFull(school)) {
      school.isSelected = false;
      return;
    }

    this.deselect();
    school.isSelected = true;
  }

  buildSchoolName(school): string {
    const name = school.name;
    const country = school.alpha_two_code;

    return `${name} (${country})`;
  }

  buildSchoolLinkDisplay(school): string {
    const linkName = school.domains[0];
    return linkName.charAt(0).toUpperCase() + linkName.slice(1);
  }

  private addEnrolledStudents(selectedStudents: any[]): void {
    const schoolName = this.getSelectedSchool().name;
    let schoolStudents = this.schoolStudentMap[schoolName];

    if (!schoolStudents) {
      this.schoolStudentMap[schoolName] = schoolStudents = [];
    }

    schoolStudents.push(...selectedStudents);
  }

  private subscribeToSchools(): void {
    this.schoolService.data
      .subscribe(schools => this.schools = schools);
  }
}
