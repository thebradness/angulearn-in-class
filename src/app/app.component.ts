import {Component, ViewChild} from '@angular/core';
import {StudentChooserComponent} from "./student-chooser/student-chooser.component";
import {SchoolChooserComponent} from "./school-chooser/school-chooser.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'AdmissionsCounselor';
  appState = {};
}
